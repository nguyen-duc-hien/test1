@extends('admin.master')
@section('admin')
    <div class="container-full">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xl-3 col-6">
                    <div class="box overflow-hidden pull-up">
                        <div class="box-body">
                            <div class="icon bg-primary-light rounded w-60 h-60">
                                <i class="text-primary mr-0 font-size-24 mdi mdi-chart-line"></i>
                            </div>
                            <div>
                                <p class="text-mute mt-20 mb-0 font-size-16">Total Buy</p>
                                <h3 class="text-white mb-0 font-weight-500">{{$totalInput}} <small class="text-success">
                                        <i class="fa fa-caret-up"></i> +2.5%</small></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-6">
                    <div class="box overflow-hidden pull-up">
                        <div class="box-body">
                            <div class="icon bg-warning-light rounded w-60 h-60">
                                <i class="text-warning mr-0 font-size-24 mdi mdi-chart-line"></i>
                            </div>
                            <div>
                                <p class="text-mute mt-20 mb-0 font-size-16">Total Sold </p>
                                <h3 class="text-white mb-0 font-weight-500">{{$totalSold}} <small class="text-success">
                                    <i class="fa fa-caret-up"></i> +10%</small></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-6">
                    <div class="box overflow-hidden pull-up">
                        <div class="box-body">
                            <div class="icon bg-info-light rounded w-60 h-60">
                                <i class="text-info mr-0 font-size-24 mdi mdi-chart-line"></i>
                            </div>
                            <div>
                                <p class="text-mute mt-20 mb-0 font-size-16">Total Selling</p>
                                <h3 class="text-white mb-0 font-weight-500">{{$totalOrderPrice}} <small class="text-danger">
                                    <i class="fa fa-caret-down"></i> -6%</small></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-6">
                    <div class="box overflow-hidden pull-up">
                        <div class="box-body">
                            <div class="icon bg-light rounded w-60 h-60">
                                <i class="text-white mr-0 font-size-24 mdi mdi-chart-line"></i>
                            </div>
                            <div>
                                <p class="text-mute mt-20 mb-0 font-size-16"> Sale / Buy </p></p>
                                <h3 class="text-white mb-0 font-weight-500">{{$totalOrderPrice - $totalInput }}<small class="text-success">
                                    <i class="fa fa-caret-up"></i> +5%</small></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="box">
                        <div class="box-header">
                            <h4 class="box-title align-items-start flex-column">
                                Upper Sale
                                <small class="subtitle">on selling and selling off</small>
                            </h4>
                        </div>
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-border">
                                    <thead>
                                        <tr class="text-uppercase bg-lightest">
                                            <th style="min-width: 250px"><span class="text-white">Images</span></th>
                                            <th style="min-width: 100px"><span class="text-fade">Product Name</span></th>
                                            <th style="min-width: 100px"><span class="text-fade">Sale-price</span></th>
                                            <th style="min-width: 150px"><span class="text-fade">quantity</span></th>
                                            <th style="min-width: 130px"><span class="text-fade">status</span></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($product as $item)
                                            <tr role="row" class="odd">
                                                <td><img src="{{ asset($item->product_thumbnail) }}" style="max-height: 150px;"></td>
                                                <td>{{ $item->product_name }}</td>
                                                <td>{{ $item->selling_price }}</td>
                                                <td>{{ $item->product_quantity }}</td>
                                                <td>
                                                    @if($item->status == '1')
                                                        <span class="badge badge-success">Active</span>
                                                    @else
                                                        <span class="badge badge-danger">Inactive</span>
                                                    @endif
                                                </td>
                                            </tr>
                                                @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
